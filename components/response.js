'use strict';

exports.ok = function(values, res) {

    var data = {
        'message': 'Operation success',
        'values': values
    };

    res.status(200);
    res.json(data);
    res.end();

}

exports.notfound = function(values, res) {

    var data = {
        'message': values
    };

    res.status(404);
    res.json(data);
    res.end();

}

exports.badrequest = function(values, res) {

    var data = {
        'message': values,
        'advice': "Please read the documentation carefully. :)"
    }

    res.status(400);
    res.json(data);
    res.end();
    
}

exports.unauthorized = function(values, res) {

    var data = {
        'message': values
    }

    res.status(401);
    res.json(data);
    res.end();
    
}

exports.internalerror = function(res, values = "Internal system failure.") {

    var data = {
        'message': values
    }

    res.status(500);
    res.json(data);
    res.end();
    
}