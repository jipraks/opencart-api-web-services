require('dotenv').config()
var express = require('express'),
    app         = express(),
    bodyParser  = require('body-parser'),
    bearerToken = require('express-bearer-token');

var cors        = require('cors');
var http        = require('http');

var httpServer  = http.createServer(app);

app.use(bodyParser.urlencoded({ extended: true, limit: '5mb' }));
app.use(bodyParser.json({ limit: '5mb' }));
app.use(bearerToken());
app.use(cors());
app.use(function (error, req, res, next) {
    if(error instanceof SyntaxError) { 
      return res.status(500).send({message : "Invalid data structure. Please read documentation carefully."});
    } else {
      next();
    }
});
app.disable('x-powered-by');

var routes  = require('./routes');
routes(app);

httpServer.listen(process.env.SERVICE_PORT);

console.log('API server started on ' + process.env.SERVICE_URL + ':' + process.env.SERVICE_PORT);