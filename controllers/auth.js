'use strict';

var response    = require('../components/response');
var model       = require('../models/auth');
var basicAuth   = require('basic-auth');

exports.login = function(req, res) {

	var authorization = basicAuth(req);
	
	if (!authorization || !authorization.name || !authorization.pass) {
		
		response.unauthorized('Unauthorized', res);
		
	} else {
		
		model.login(authorization, function(result, comment){

			if(result[0] == 200) {
                response.ok(result[1], res);
            } else if(result[0] == 401) {
                response.notfound('Invalid username / password', res);
            } else if(result[0] == 500) {
                response.internalerror(res);
            }

		});
		
	}

};