var mysql = require('mysql');

var maindb = mysql.createConnection({
    host: process.env.MAINDB_HOST,
    port: process.env.MAINDB_PORT,
    user: process.env.MAINDB_USER,
    password: process.env.MAINDB_PASS,
    database: process.env.MAINDB_NAME
});

maindb.connect(function (err) {
    if(err) throw err;
});

module.exports = maindb;