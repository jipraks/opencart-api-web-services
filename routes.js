'use strict';

module.exports = function (app) {

  var auth = require('./controllers/auth');

  var index = function (req, res, next) {

    res.sendStatus(404);

  }

  var authneed = function (req, res, next) {

    if (typeof req.token !== 'undefined') {

      var jwt = require('jsonwebtoken');

      jwt.verify(req.token, process.env.JWTKEY, function (error, authData) {

        if (error) {

          res.sendStatus(401);

        } else {

          var currentDate = Math.round((new Date()).getTime() / 1000);

          if (authData.expired < currentDate) {

            res.status(401);
            res.json({ 'message': 'Token expired' });
            res.end();

          } else {

            req.user_id = authData.user_id;
            req.contact_name = authData.contact_name;

            next();

          }

        }

      });

    } else {

      res.sendStatus(401);

    }

  }

  app.all('/', index);
  app.all('/v1/*', authneed);

  app.route('/login')
    .get(auth.login);

};