'use strict';

var maindb      = require('../config/maindb');
var jwt         = require('jsonwebtoken');

exports.login = function(req, res) {

    var username = req.name;
    var password = req.pass;

    maindb.query("SELECT * FROM " + process.env.MAINDB_PREFIX + "user WHERE username = '" + username + "' AND password = SHA1(CONCAT(`salt`,SHA1(CONCAT(`salt`,SHA1('" + password + "'))))) AND status = 1", function(error, rows, field){

        if(!error) {

            console.log(rows.length);

            if(rows.length > 0) {

                var user_id         = rows[0].user_id;
                var user_group_id   = rows[0].user_group_id;
                var username        = rows[0].username;
                var firstname       = rows[0].firstname;
                var lastname        = rows[0].lastname;
                var email           = rows[0].email;
                var expired         = Math.round((new Date()).getTime() / 1000)+86400; // Add token expiry time. 24 hours from first generated.
                var token           = jwt.sign({
                    user_id: user_id, 
                    username: username,
                    firstname: firstname,
                    lastname: lastname,
                    email: email
                }, process.env.JWT_KEY);

                var result = {
                    user_id: user_id,
                    user_group_id: user_group_id,
                    username: username,
                    firstname: firstname,
                    lastname: lastname,
                    email: email,
                    expired: expired,
                    token: token
                };

                res([200, result]);

            } else {

                res([401, "Wrong credentials"]);

            }

        } else {

            console.log(error);
            res([500]);

        }

    });

};
